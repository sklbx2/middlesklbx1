#include <iostream>
#include <string>
#include <vector>
#include <memory>


// 1. �������
/*

��������� ��������������� ������ ��� ������, ����� ��������� �������������� �������� � ���������� ���������, ��������� ��� ��������� ��������, � ����� ��������� ������ ��������.

*/

// ������:
/*

����� ��������� �������. ������ ���� ����� ��������� ��������� ��������, ����� ��� ���������, ���������� � �. �. 
�� ����� ������������ ������� "�������", ����� ��������������� ��� �������� � ������� ������, ������� ����� ���������� � ����� ��� ����������.

*/

// ��������� ������� - ����� �������
class Order 
{
public:
    virtual void execute() = 0;
};

// ���������� ������� ���������
class AttackOrder : public Order 
{
public:
    void execute() override 
    {
        std::cout << "Warrior is attacking!\n";
    }
};

// ���������� ������� ������
class DefendOrder : public Order 
{
public:
    void execute() override 
    {
        std::cout << "Warrior is defending!\n";
    }
};

// ����� ������, ������� ����� ��������� �������
class WarriorSquad 
{
    // ������ ���� ��������
    std::vector<Order*> m_orders;

public:
    void addOrder(Order* Order) 
    {
        m_orders.push_back(Order);
    }

    void executeOrders() 
    {
        for (Order* Order : m_orders) 
        {
            Order->execute();
        }
    }
};


///////////////////////
void Command()
{
    WarriorSquad squad;

    squad.addOrder(new AttackOrder);    // ��������� � ���������� ������. � ������ ���� ��� �� �������������
    squad.addOrder(new DefendOrder);    // ��������� � ���������� ������. � ������ ���� ��� �� �������������

    squad.executeOrders();
}



// 2. �����������
/*

��������� �������� ��������� ������ ������� �� � ��������
������� ����� ���������� ����� "���� �� ������" ����� ���������, ��� ��������� � ����� ������� ������ ������ �� ��������� ������ ��������

*/

// ������:
/*

����� ��������� ���� ������� (��� NPC) � ���, ��� ��� ���������

*/

// ��������� �����������
class IObserver 
{
public:
    virtual void onDoorOpened() = 0;
};

// ����� ������������ �������� - �����
class Door 
{
    // ������ ������������
    std::vector<IObserver*> m_observers;
    bool bOpen;

public:
    void attach(IObserver* observer)
    {
        m_observers.push_back(observer);
    }

    void notify() 
    {
        for (IObserver* observer : m_observers)
        {
            observer->onDoorOpened();
        }
    }

    void openDoor() 
    {
        bOpen = true;
        notify();
    }
};

// ����� ����������� - �����
class Player : public IObserver
{
private:
    std::string m_name;

public:
    Player(std::string playerName) : m_name(playerName) {}

    void onDoorOpened() override
    {
        std::cout << m_name << " sees that the door is now open and can proceed further!\n";
    }
};


///////////////////////
void Observer()
{
    Door door;
    Player player1("Patrick");
    Player player2("Bob");

    door.attach(&player1);
    door.attach(&player2);

    door.openDoor();
}


// 3. ����������
/*

��������� ��������� ����� �������� � �������� ��� ��������� �� �������.

*/

// ������:
/*

���� ������� ���, ������� ������� �� ��������� ��������, ����� ��� ������, ���������, �������� � �. �. 
�� ����� �������� ���������� � ������ �������, �� ��� ��������� ������� ����� ��������. 
�� ����� ������������ ������� "����������", ����� �������� �������� ���������, �� ���������� �������� ������ ��������.

������ �������� ������ ������� ��������� ���� ��� - ����������� � ��� ��������� ��� ����� ����������.
�� ������ �������������� ����� ����� ��� ������������� ������ ������ � ����� ������ ����������, ��� �� ������ ��� �������� ������

*/

class Character;

// ��������� ����������
class IVisitor 
{
public:
    virtual void visitBuilding() = 0;
    virtual void visitCharacter(const Character& character) = 0;
};

// ���������� ����� ���������� - �����������
class ObjectViewer : public IVisitor
{
public:
    void visitBuilding() override 
    {
        std::cout << "Building visited for viewing.\n";
    }

    void visitCharacter(const Character& character) override
    {
        std::cout << "Character visited for viewing.\n";
        // ... �����-�� ������������� ������������� � ���������� ����������
    }
};

// ��������� ������� �������� ����, ������� ����� ��������� ����������
class IObject 
{
public:
    virtual void accept(IVisitor& visitor) = 0;
};

// ���������� ������ ��������
class Building : public IObject
{
public:
    void accept(IVisitor& visitor) override
    {
        visitor.visitBuilding();
    }
};

class Character : public IObject
{
public:
    void accept(IVisitor& visitor) override
    {
        visitor.visitCharacter(*this);
    }
};

///////////////////////
void Visitor() 
{
    ObjectViewer viewer;

    Building building;
    Character character;

    building.accept(viewer);
    character.accept(viewer);

}


// 4. ���������
/*

��������� ������� �������� ���� ��������� � ����������� �� ����������� ���������.
�������, ����� ���������� ��������� ������� ����� �������� � ������ �� ��� ���������
� ����� ���������� �������� ������� �������� ����������, ��������� � ���������� ����������� �������

���, ������, ������������ ��-�� ����������� ��� ������� ���������. ��� ��� ���������� �������� ����� ���������� �� �����

*/

// ������:
/*

 �������� �������, ������� ����� ���������� � ��������� ���������� � ����������� �� ������� ������� � �����. 
 �� ����� ������������ ������� "���������", ����� ����������� ��������� ��������� �������� (��������, "��� �������", "������������ �����", "����� � �������") 
 � �������� ��� ��������� � ������������ � ������� ����������.

*/

// ����������� ���������, � ������� ����������� ��������, ��������� �� ���������
class IState 
{
public:
    virtual void insertMoney() = 0;
    virtual void ejectMoney() = 0;
    virtual void selectProduct() = 0;
    virtual void dispenseProduct() = 0;
};

// ���������� ��������� - "��� ������"
class NoProductState : public IState 
{
public:
    void insertMoney() override 
    {
        std::cout << "No product available to select.\n";
    }

    void ejectMoney() override 
    {
        std::cout << "No money to return.\n";
    }

    void selectProduct() override 
    {
        std::cout << "No product available to select.\n";
    }

    void dispenseProduct() override 
    {
        std::cout << "No product available to dispense.\n";
    }
};

// ���������� ��������� - "����� � �������"
class ReadyState : public IState 
{
public:
    void insertMoney() override 
    {
        std::cout << "Money inserted.\n";
    }

    void ejectMoney() override 
    {
        std::cout << "Money returned.\n";
    }

    void selectProduct() override 
    {
        std::cout << "Product selected.\n";
    }

    void dispenseProduct() override 
    {
        std::cout << "Product dispensed.\n";
    }
};

// ����� ��������� ��������
class VendingMachine 
{
    // ��������� ��������
    IState* m_currentState;

public:
    VendingMachine() 
    {
        m_currentState = new NoProductState();
    }

    void setState(IState* newState) 
    {
        m_currentState = newState;
    }

    void insertMoney() 
    {
        m_currentState->insertMoney();
    }

    void ejectMoney() 
    {
        m_currentState->ejectMoney();
    }

    void selectProduct() 
    {
        m_currentState->selectProduct();
    }

    void dispenseProduct() 
    {
        m_currentState->dispenseProduct();
    }
};

void State()
{
    VendingMachine vendingMachine;

    // ������� ������� - �� ��������� ��������� "��� ������" ����� ���� ���������

    vendingMachine.selectProduct(); // Output: No product available to select.
    vendingMachine.insertMoney();   // Output: No product available to select.


    // �������� ���������

    vendingMachine.setState(new ReadyState());

    // ��������� ����� ������� ����� ��, ��������� ����������

    vendingMachine.insertMoney();   // Output: Money inserted.
    vendingMachine.selectProduct();  // Output: Product selected.
    vendingMachine.dispenseProduct(); // Output: Product dispensed.
}


// 5. ���������
/*

��������� ���������� ��������� ����������, ��������������� ������ �� ��� � ���������� �� ������������������.
�������, ����� ���� ��������� ��������� ���������� ��������
� ����� ����� �������� ����������� ���� ������� ����������� �������

*/

// ������:
/*

���� ��������� ���� �����, ������� ����� ������������ � ���. 
�� ����� ������������ ������� "���������" ��� ���������� ��������� ����������

*/

// ����������� ����� ��������� - ����������
class SpellStrategy 
{
public:
    virtual void castSpell() = 0;
};

// ���������� ��������� - �������� ���
class FireballSpell : public SpellStrategy 
{
public:
    void castSpell() override 
    {
        std::cout << "Casting Fireball spell!\n";
    }
};

// ���������� ��������� - ������� ������
class IceArrowSpell : public SpellStrategy 
{
public:
    void castSpell() override 
    {
        std::cout << "Casting Ice Arrow spell!\n";
    }
};

// ������� ��������, ��������� ������������ �����
class PlayerCharacter 
{
    std::unique_ptr<SpellStrategy> m_currentSpell;

public:
    PlayerCharacter(std::unique_ptr<SpellStrategy> strategy) : m_currentSpell(std::move(strategy)) {}

    void setSpell(std::unique_ptr<SpellStrategy> strategy)
    {
        m_currentSpell = std::move(strategy);
    }

    void castSpell() 
    {
        m_currentSpell->castSpell();
    }
};

void Strategy() 
{
    PlayerCharacter player(std::make_unique<FireballSpell>());
    player.castSpell(); // Output: Casting Fireball spell!

    player.setSpell(std::make_unique<IceArrowSpell>());
    player.castSpell(); // Output: Casting Ice Arrow spell!

}